<?php
// *****
// ***** LSBex *****
// *****
// Little Systems (Page) Builder Express
class LSBex {

  function __construct($lsInt) {
    $this->lsInt  = $lsInt;
    $this->help   = (isset($_GET["help"])) ? true : false; // option to save help state, and with help true by default.

    if (isset($_POST["ajax"])) {
      $this->ajax();
    } else {
      $this->show_builder_express();
    }
  }

  function ajax() {
    if ($_POST["lsbex"]=="save_page_order") {
      if ($this->lsInt->user["savePageOrder"]) {
        $this->save_page_order();
      }
    }
  }

  function show_settings() {
    // ***** SECURITY: post_id *****

    $post_id          = (int) $_GET["post"];

    $post             = get_post( $post_id );
    $post_type        = $post->post_type;
    $post_type_object = get_post_type_object( $post_type );

    wp_enqueue_script('post'); // edit-form-advanced

    require_once( ABSPATH . "wp-admin/includes/meta-boxes.php" );

    post_submit_meta_box($post, $args = array() );


#print_r($post);

#wp_enqueue_script('post');

#    echo "Settings: ".$post_id;
    echo "<input id='post_id' type='hidden' value='".$post_id."'>";


  }

  function save_page_order() {
    $pages = json_decode(stripslashes($_POST["pages"]), true);

    $order=0;
    foreach ($pages as $page) {
      $pid = intval($page["parent_id"]);
      $id  = intval($page["item_id"]);
      if (!$pid) {
        $pid=0;
      }
      $hash[$pid][$order][$id]=1;
      $order++;
    }

    $stem=0;
    $this->lsInt->save_page_order($hash, $stem);
  }

  function show_builder_express() {
    $post       = (isset($_GET["post"]))            ? (int) $_GET["post"]           : "";
    $action     = (isset($_REQUEST["action"]))      ?       $_REQUEST["action"]     : "pages";
    $subAction  = (isset($_REQUEST["sub_action"]))  ?       $_REQUEST["sub_action"] : "";
    $help       = $this->help;

    echo "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>\n";
    echo "<!-- (Page) Builder Express for Wordpress.  Copyright Little Systems 2015 -->\n\n";

    echo "<html>\n\n";

    include(LSBEXROOT."/includes/head.php");

    echo "<body>\n\n";

    include(LSBEXROOT."/includes/banner.php");

    echo "<div class='wrap' id='row6'>\n";
    echo "  <span class='col1' id='main'>\n";

    if ($action=="pages") {
      if ($subAction=="create") {
        $this->lsInt->create_page($_POST["page_title"]);
      }
      $this->show_pages();
    } elseif ($action=="settings") {
      $this->show_settings();
    } elseif ($action=="edit") {
      $this->show_edit_page();
    }

    echo "  </span><span class='col2'>\n";
    echo "    <img height='16' id='lsbex_saving' src='".admin_url("/images/wpspin_light.gif")."' style='display:none' width='16'></span>\n";
    echo "</div>\n\n";

    include(LSBEXROOT."/includes/help.php");

    echo "</body>\n\n";
    echo "</html>";
  }

  function show_edit_page() {
    if (isset($_GET["post"])) {
      $id = (int) $_GET["post"];
    } else {
      echo "<script> $('document').ready(function() { $('#page_button').trigger('click'); }); </script>";
      exit;
    }

    $post             = get_post( $id );
    $post_type        = $post->post_type;
    $post_type_object = get_post_type_object( $post_type );

    $editing = true;

    if ( ! $post )
      die( __( 'You attempted to edit an item that doesn&#8217;t exist. Perhaps it was deleted?' ) );

    if ( ! $post_type_object )
      die( __( 'Unknown post type.' ) );

// requires further condition for being the page owner.
#    if ( ! current_user_can( 'edit_post', $id ) )
#      die( __( 'You are not allowed to edit this item.' ) );

    if ( 'trash' == $post->post_status )
      die( __( 'You can&#8217;t edit this item because it is in the Trash. Please restore it and try again.' ) );


    $actionPath = '?'.$_SERVER["QUERY_STRING"];
?>

    <div id="edit_create">
      <form action="<?=$actionPath;?>" method="post" name="page_create">
        <p><input id='title' type='text' value='<?php echo $post->post_title ?>'></p>
        <textarea id='content'></textarea>
        <p><input type='submit'></p>
      </form>
    </div>
<?php
  }

  function show_pages() {
    $page = $this->lsInt->get_page_hash();
    $help = $this->help;

    $actionPath = "./";
    if ($help) $actionPath .= "?" . $this->helpOn;

    if ($page) {
      $this->show_pages_branch($page,0);
    } else {
      print '    <p>There are no pages.</p>'.PHP_EOL;
    }
?>
    <form action="<?=$actionPath;?>" method="post" name="page_create">
      <input name="action" type="hidden" value="pages">
      <input name="sub_action" type="hidden" value="create">

      <div id="page_create">
        <input name="page_title" placeholder="Page Name" type='text'>
        <span class="space">&nbsp;</span><span class="redslideout">&nbsp;</span><input class="button" type="submit" value="Create"></div>
    </form>

<?php
  }

  function show_pages_branch($hash, $stem, $level=2) {
    // parse the hash to HTML (indented)

    $savePageOrder  = $this->lsInt->user["savePageOrder"];
    $indent         = "  ";
    $s              = str_repeat($indent, $level);
    $editAllPages   = $this->lsInt->user["editAllPages"];
    $userId         = $this->lsInt->user["id"];

    if (isset($hash[$stem])) {
      if ($stem==0) {
        if ($savePageOrder) {
          $class = 'sortable';
          print "\n$s<form name='pages'>\n";
        } else {
          $class = 'unsortable';
        }
        $s = str_repeat($indent, ++$level);
        print "$s<ol id='pages' class='$class'>\n";
      } else {
        print "\n$s<ol>\n";
      }
      ksort($hash[$stem]);
      $help = (isset($_GET["help"])) ? "&help=on" : "";
      foreach ($hash[$stem] as $order => $page) {
        $s = str_repeat($indent, ++$level);
        foreach ($page as $id => $attribute) {
          $class = ($attribute["nesting"]) ? '' : ' class="mjs-nestedSortable-no-nesting"';
          print "$s<li$class id='list_$id' value='$id'>";
          print '<div><span class="title">';
          if ($editAllPages || ($editThisPage = ($attribute["pageAuthor"] == $userId))) {
            print "<a class='page' href='./?post=$id&action=edit$help'>";
          } else {
            print '<span class="inactive">';
          }
          ($attribute["pageTitle"]) ? print $attribute["pageTitle"] : print '(no title)';
          ($editAllPages || $editThisPage) ? print '</a>' : print '</span>';
          print '</span>';
          $pageStatus = $attribute["pageStatus"];
          if ($pageStatus<>"publish" && ($editAllPages || $editThisPage)) {
            print '<span class="status"> - '.ucfirst($pageStatus).'</span>';
          }
          print '</div>';
          $this->show_pages_branch($hash, $id, $level);
          print "$s</li>\n";
        }
        $s = str_repeat($indent, --$level);
      }
      print "$s</ol>\n";
      if ($stem==0 && $savePageOrder) {
        $s = str_repeat($indent, --$level);
        print "$s</form>\n\n";
      }
    } else {
      print PHP_EOL;
    }
  }
}
