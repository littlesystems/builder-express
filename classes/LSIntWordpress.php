<?php
// *****
// ***** LSIntWordpress *****
// *****
// Little Systems Interface to Wordpress
class LSIntWordpress extends LSInt {

  function get_user_hash() {

    $currentUser = get_userdata(get_current_user_id());
    $hash["id"]   = $currentUser->ID;
    $hash["name"] = $currentUser->display_name;

    $currentUser = wp_get_current_user(); $roles = $currentUser->roles; $role = array_shift($roles);
    $hash["role"] = $role;

    $savePageOrderRoles = array("super_admin_menu","administrator","editor");
    $hash["savePageOrder"]  = in_array($role, $savePageOrderRoles);

    $hash["editAllPages"]   = $hash["savePageOrder"];

    return $hash;
  }

  function get_site_hash() {

    $hash["name"] = get_bloginfo(); // default is name

    return $hash;
  }

  function get_page_hash() {
    $nestingStatus = array('publish');

    $pages = get_pages(array('post_status' => 'publish,future,draft,pending,private')); // Wordpress get_pages function.

    $hash  = array();  // in case no pages.

    foreach ($pages as $page) {
     $hash[$page->post_parent][$page->menu_order][$page->ID] = array(
        "pageAuthor" => $page->post_author,
        "pageTitle"  => $page->post_title,
        "pageStatus" => $page->post_status,
        "nesting"    => (in_array($page->post_status, $nestingStatus)) ? true : false
      );
    }
    return $hash;
  }

  function save_page_order($hash, $stem) {
    global $wpdb;
    $menu_order=10;
    if (isset($hash[$stem])) {
      ksort($hash[$stem]);
      foreach ($hash[$stem] as $order => $page) {
        foreach ($page as $id => $val) {
          if ($id && current_user_can('edit_post', $id)) {
            $wpdb->update(
              $wpdb->prefix."posts",
              array("post_parent" => $stem, "menu_order" => $menu_order),
              array("ID" => $id)
            );
            $this->save_page_order($hash,$id);
            $menu_order=$menu_order+10;
          }
        }
      }
    }
  }

  function create_page($title) {
    $array["post_name"]   = wp_unique_post_slug(sanitize_title($title), '', 'draft', 'page', false);;
    $array["post_title"]  = $title;
    $array["post_status"] = "draft";
    $array["post_type"]   = "page";
    $array["post_author"] = get_current_user_id();
    $array["menu_order"]  = 9999999999;

    wp_insert_post($array);
  }

  function get_page_uri($id=null) {
    $uri = get_page_uri($id);
    return $uri;
  }
}
