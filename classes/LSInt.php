<?php
// *****
// ***** LSInt *****
// *****
// Little Systems Interface (parent class)
class LSInt {

  function __construct() {
    $this->user     = $this->get_user_hash();
    $this->site     = $this->get_site_hash();
    $this->page     = $this->get_page_hash();
  }

  function get_user_hash() { // returns hash
    // name
    // role (CMS, original)
    // savePageOrder (can user save page order?)
    // editAllPages (can user edit all pages?)
  }

  function get_site_hash() { // returns hash
  }

  function get_page_hash() { // returns hash
    // pageTitle
    // pageStatus
    // nesting
  }

  function save_page_order($hash, $stem) {
  }

  function create_page($title) {
  }

  function get_page_uri($id=null) {
  }
}
?>
