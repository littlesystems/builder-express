<?php
// do not load directly
if (!defined('ABSPATH'))
  die();

// auto-load classes
spl_autoload_register(function ($class) {
  include "classes/$class.php";
});

// settings 
define('CMS',       'wordpress');
define('LSBEXPATH', '/wp-content/plugins/builder-express');
define('LSBEXROOT', $_SERVER["DOCUMENT_ROOT"].LSBEXPATH);

// interface class name
$lsInterface = "LSInt".ucfirst(CMS);
$lsInt = new $lsInterface();

$lsBex = new LSBex($lsInt);
?>
