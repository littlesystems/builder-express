<?php
  if ($action=="pages") {
    $wordpressPath  = "edit.php?post_type=page";
    $pagesButton    = "button_yellow";
    $settingsButton = "button_grey";
    $editButton     = "button_grey";

    $pageAnchor     = false;

  } elseif ($action=="settings") {
    $wordpressPath  = "post.php?post=$post&action=edit";
    $pagesButton    = "button_tan";
    $settingsButton = "button_yellow";
    $editButton     = "button_tan";

    $pageAnchor     = true;

  } elseif ($action=="edit") {
    $wordpressPath  = "post.php?post=$post&action=edit";
    $pagesButton    = "button_tan";
    $settingsButton = "button_tan";
    $editButton     = "button_yellow";

    $pageAnchor     = true;
  }
 
  $this->helpOn = $helpOn = "help=on"; 

  $helpState      = ($help)       ? $helpOn                             : "";
  $helpSwitch     = ($help)       ? ""                                  : $helpOn;

  $path           = ($action)     ? "?post=$post"                       : "";
  $viewPath       = ($path)       ? $this->lsInt->get_page_uri($post)   : "";
  $helpPath       = ($path)       ? "$path&action=$action"              : "";
  $settingsPath   = ($path)       ? "$path&action=settings"             : "";
  $editPath       = ($path)       ? "$path&action=edit"                 : "";

  $path          .= ($help)       ? (($path) ? "&" : "?") . $helpState  : "";
  $helpPath      .= ($helpSwitch) ? (($path) ? "&" : "?") . $helpSwitch : "";
  $settingsPath  .= ($help)       ? "&$helpOn"                          : "";
  $editPath      .= ($help)       ? "&$helpOn"                          : "";

  $pagesPath      = ($help)       ? "?$helpOn"                          : "";

?>
<div id="row1">
  <span class="col1"><a id="returntowordpress" href="./<?=$wordpressPath?>">Wordpress</a></span><span class="col2">&nbsp;<!--User: <?php #$this->lsInt->user["name"]?> (<?php ucfirst($this->lsInt->user["role"])?>)--><!--<a href="./edit.php">Posts</a>--></span>
</div>

<div id="row2">
  <span class="col1"><span class="heading">Builder Express</span><span class="blueslidein">&nbsp;</span></span><span class="col2"><a class="button button_round button_yellow" href="./<?=$helpPath?>" id="button_help">Help</a> <a class="button button_round button_green" href="/<?=$viewPath?>" id="button_view">View</a></span>
</div>

<div id="row3">
  <span class="col1"><a class="button button_slider <?=$pagesButton?>" href="./<?=$pagesPath?>" id="button_pages">Pages</a><a class="button button_slider <?=$settingsButton?>"<?php if ($pageAnchor) echo " href='./$settingsPath'"; ?> id="button_settings">Settings</a><a class="button button_slider <?=$editButton?>"<?php if ($pageAnchor) echo " href='./$editPath'"; ?> id="button_edit">Edit</a></span><span class="col2">&copy; Little Systems</span>
</div>

<div <?php if ($help) echo 'class="help" '; ?>id="row4">
  <span class="col1"></span><span class="col2"></span>
</div>

<?php if ($help) { ?>
<div class='help' id='row5'>
  <span class='col1'>&nbsp;</span><span class='col2'><b>Training Mode</b> see below for help</span>
</div>
<?php } else { echo "<!-- (row5) help, off -->\n\n"; }?>
