<head>

<title><?=$this->lsInt->site["name"]?> | Wordpress Builder Express</title>

<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />

<link rel="stylesheet" type="text/css" href="<?=LSBEXPATH?>/css/be.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?=LSBEXPATH?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?=LSBEXPATH?>/js/jquery.mjs.nestedSortable.js"></script>
<script src="<?=LSBEXPATH?>/js/builderexpress.js"></script>
<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>

</head>

