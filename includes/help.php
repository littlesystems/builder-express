<?php 
  if ($help) {
    print '<div class="help" id="row7">'.PHP_EOL;
    print '  <span class="col1">'.PHP_EOL;

    if ($action=="pages") {
      print '    <p><b>Pages</b></p>'.PHP_EOL;
      print '    <p><i>Create and arrange pages.</i></p></span>';
    } elseif ($action=="settings") {
      print '</span>';
    } elseif ($action=="edit") {
      print '    <p><b>Edit</b></p>'.PHP_EOL;
      print '    <p><i>Edit the page title and content.</i></p></span>';
    }

    print '<span class="col2">'.PHP_EOL;
    print '    <img height="77" id="bird" src="'.LSBEXPATH.'/images/help/bird.gif" width="68"><br clear="all">'.PHP_EOL;
    print '    <span class="text">'.PHP_EOL;
    print '      <p>Logging in Guide (printable)</p>'.PHP_EOL;
    print '    </span>'.PHP_EOL;
    print '  </span>'.PHP_EOL;
    print '</div>'.PHP_EOL;
  } else { 
    print '<!-- (row7) help, off -->'.PHP_EOL.PHP_EOL; 
  } 
?>
