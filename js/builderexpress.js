$(document).ready(function(){
  var slider_path            = "/wp-content/plugins/builder-express/images/slider";

  // Pages - nestedSortable initialise
  $('ol.sortable').nestedSortable({
    handle: 'div',
    items: 'li',
    toleranceElement: '> div',
    forcePlaceholderSize: true,
    placeholder: 'placeholder',
    opacity: .6,
    revert: 250,
    maxLevels: 7,
    update: function() { save_order(); }
  });

  $('ol.sortable li, ol.static li').click(function(event) {
    if (event.target.tagName != 'LI') return;
    window.location.href = "./?post=" + this.value + "&action=settings";
  });
});

// Pages - Save Page Order
function save_order() {

//  if ($('#lsbex_saving').is(":hidden")) {
  $('#lsbex_saving').show();

  arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});

  var data = {
    action: "lsbex",
    ajax: true,
    lsbex: "save_page_order",
    pages: JSON.stringify(arraied) 
  };
  $.post('./admin-ajax.php', data, function(response) {
    $('#lsbex_saving').hide();
//      alert(response);
  });
//  }
}
