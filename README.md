# (Page) Builder Express user-interface plugin working prototype for Wordpress #

Copyright (c) 2015 Little Systems (http://littlesystems.com.au).  November 2015.

You may distribute under the terms of the Artistic License 2.0, as specified in the README.license file.

### What is this repository for? ###

**A working prototype user-interface for WordPress (and potentially other CMSs), and with parallel links to the existing interface.**  The user-interface is for end-users (such as editors, authors and contributors) of page-intensive sites

**The plugin features:**

* An intuitive page creation, ordering and editing process,
* An abstraction of functions (for subsequent interfacing with other CMS’s, such as Joomla, Drupal and Magento),

### How do I get set up? ###

The repository installs as a Wordpress plugin.

### Screenshots ###

**Image 1:** Click “WordPress” at any time to jump to the equivalent screen in WordPress.
![PageBuilderExpress_pages.png](https://bitbucket.org/repo/77rK6k/images/317005578-PageBuilderExpress_pages.png)

**Image 2:** A drag and drop page rearrangement in progress.
![PageBuilderExpress_pages_dragging.png](https://bitbucket.org/repo/77rK6k/images/2025765283-PageBuilderExpress_pages_dragging.png)

**Image 3:** Non-editable pages shaded out.
![PageBuilderExpress_pages_author_contributor.png](https://bitbucket.org/repo/77rK6k/images/1589506638-PageBuilderExpress_pages_author_contributor.png)

### Who do I talk to? ###

* Please contact [Little Systems](https://littlesystems.com.au) enquiries, comments or support.