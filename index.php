<?php
/*
Plugin Name: Builder Express
Plugin URI: https://bitbucket.org/littlesystems/builder-express/
Description: (Page) Builder Express for Wordpress.  User-interface for editors, authors and contributors.  Suitable for page-based sites, with multiple users.  In development.
Author: Little Systems.
Version: 1.0 (October, 2015)
Author URI: http://www.littlesystems.com.au
*/

// do not load directly
if ( !defined('ABSPATH') )
        die();

add_action('current_screen','lsbex_redirect',1000);

// redirect upon decision and/or setting to use the Builder Express interface.
// else, exit (carry on) to the CMS.
function lsbex_redirect() {
  if (!current_user_can( 'manage_options' )) {
    if (($_SERVER["REQUEST_URI"]=="/wp-admin/")||(substr($_SERVER["REQUEST_URI"],0,11)=="/wp-admin/?")) {
      include(dirname(__FILE__).'/builderexpress.php');
      exit;
    }
  }
}

function lsbex_ajax() {
  include(dirname(__FILE__).'/builderexpress.php');
  exit;
}
add_action('wp_ajax_lsbex', 'lsbex_ajax');

/*
add_action('admin_menu', 'lsbex_admin_page');

function lsbex_admin_page() {
  $lsbex_settings = add_options_page(__('Builder Express', 'lsbex'), __('Builder Express', 'lsbex'), 'manage_options', 'builder-express', 'lsbex_display_admin_page');
}

function lsbex_display_admin_page() {
?>
  <div class="wrap">
    <h2><?php _e('Builder Express', 'lsbex'); ?></h2>
  </div>
<?php
}
*/
?>
